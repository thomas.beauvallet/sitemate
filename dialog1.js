'use strict';

// Extend the HTMLElement class to create the web component
class ConfirmDialog extends HTMLElement {
  constructor() {
    super();
    // console.log('Created', this);
    this.styleElement = null;
    this.modalOpenButton = null;
    this.modalElement = null;
    this.createStyles();
  }

  createStyles() {
    const styles = document.querySelectorAll('style.modal-confirm');
    if (styles.length > 0) return;
    this.styleElement = document.createElement('style');
    this.styleElement.className = 'modal-confirm';
    this.styleElement.innerHTML = `
      .modal {
        position: fixed;
        width: 100vw;
        height: 100vh;
        opacity: 0;
        visibility: hidden;
        top: 0;
        left: 0;
        display: flex;
        align-items: center;
        justify-content: center;
      }
      .modal.open {
        visibility: visible;
        opacity: 1;
      }
      .modal-overlay {
        position: absolute;
        width: 100%;
        height: 100%;
        background: rgba(0, 0, 0, 0.5);
        transition: opacity 200ms;
      }
      .modal-content {
        border-radius: 10px;
        background: #fff;
        position: relative;
        padding: 30px;
      }
      .button {
        display: inline-block;
        font-weight: 400;
        text-align: center;
        white-space: nowrap;
        vertical-align: middle;
        -webkit-user-select: none;
        -moz-user-select: none;
        -ms-user-select: none;
        user-select: none;
        border: 1px solid transparent;
        padding: 0.375rem 0.75rem;
        font-size: 1rem;
        line-height: 1.5;
        border-radius: 0.25rem;
        cursor: pointer;
      }
    `;
    const head = document.querySelector('head');
    head.appendChild(this.styleElement);
  }

  createDOM() {
    const message = this.getAttribute('data-message') || 'Are you sure you want to continue?';
    // console.log('Create DOM elements');
    this.createStyles();
    this.modalOpenButton = document.createElement('button');
    this.modalOpenButton.classList.add('button');
    this.modalOpenButton.innerText = 'Click me';
    this.appendChild(this.modalOpenButton);
    this.modalElement = document.createElement('div');
    this.modalElement.className = 'modal';
    this.modalElement.innerHTML = `
      <div class="modal-overlay modal-exit"></div>
        <div class="modal-content">
          <h1>${message}</h1>
          <button class="modal-confirm modal-exit button">Yes</button>
          <button class="modal-cancel modal-exit button">Cancel</button>
        </div>
      </div>
    `;
    const resultCallback = this.getAttribute('data-result') || null;
    const exits = this.modalElement.querySelectorAll('.modal-exit');
    exits.forEach(exit => {
      exit.addEventListener('click', event => this.onCloseModal(event));
    });
    this.modalElement.querySelector('.modal-confirm').addEventListener('click', () => {
      resultCallback && window[resultCallback](true);
    });
    this.modalElement.querySelector('.modal-cancel').addEventListener('click', () => {
      resultCallback && window[resultCallback](false);
    });
    document.body.appendChild(this.modalElement);
    this.modalOpenButton.addEventListener('click', event => this.onOpenModal(event));
  }

  clearDOM() {
    this.modalOpenButton.removeEventListener('click');
    this.removeChild(this.modalOpenButton);
    const exits = this.modalElement.querySelectorAll('.modal-exit');
    exits.forEach(exit => {
      exit.removeEventListener('click');
    })
    document.body.removeChild(this.modalElement);
    const head = document.querySelector('head');
    head.removeChild(this.styleElement);
  }

  onOpenModal(event) {
    // console.log('open modal', this.modalElement, event);
    event.preventDefault();
    this.modalElement.classList.add('open');
  }

  onCloseModal(event) {
    // console.log('close modal', this.modalElement, event);
    event.preventDefault();
    this.modalElement.classList.remove('open');

  }

  connectedCallback() {
    // console.log('Added to the DOM!', this);
    this.createDOM();
  }

  disconnectedCallback() {
    // console.log('Deleted from the DOM', this);
    this.clearDOM();
  }
}

// Define the new web component
if ('customElements' in window) {
  customElements.define('confirm-dialog', ConfirmDialog);
}

