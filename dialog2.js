'use strict';

class ConfirmDialog2 {
  constructor(dialogMessage) {
    this.dialogMessage = dialogMessage || 'Are you sure you want to continue?';
    this.dialogElement = null;
    this.confirmButton = null;
    this.cancelButton = null;
    this.closeZones = [];
    this._createDialog();
  }

  _createStyles() {
    const styles = document.querySelectorAll('style.dialog-confirm');
    if (styles.length > 0) return;
    this.styleElement = document.createElement('style');
    this.styleElement.className = 'dialog-confirm';
    this.styleElement.innerHTML = `
      .dialog {
        position: fixed;
        width: 100vw;
        height: 100vh;
        opacity: 0;
        visibility: hidden;
        top: 0;
        left: 0;
        display: flex;
        align-items: center;
        justify-content: center;
      }
      .dialog.open {
        visibility: visible;
        opacity: 1;
      }
      .dialog-overlay {
        position: absolute;
        width: 100%;
        height: 100%;
        background: rgba(0, 0, 0, 0.5);
        transition: opacity 200ms;
      }
      .dialog-content {
        border-radius: 10px;
        background: #fff;
        position: relative;
        padding: 30px;
      }
      .dialog-button {
        display: inline-block;
        font-weight: 400;
        text-align: center;
        white-space: nowrap;
        vertical-align: middle;
        -webkit-user-select: none;
        -moz-user-select: none;
        -ms-user-select: none;
        user-select: none;
        border: 1px solid transparent;
        padding: 0.375rem 0.75rem;
        font-size: 1rem;
        line-height: 1.5;
        border-radius: 0.25rem;
        cursor: pointer;
      }
    `;
    const head = document.querySelector('head');
    head.appendChild(this.styleElement);
  }

  _createDialog() {
    // console.log('create dialog');
    this._createStyles();
    this.dialogElement = document.createElement('div');
    this.dialogElement.className = 'dialog';
    this.dialogElement.innerHTML = `
      <div class="dialog-overlay dialog-exit"></div>
        <div class="dialog-content">
          <h1>${this.dialogMessage}</h1>
          <button class="dialog-confirm dialog-button">Yes</button>
          <button class="dialog-cancel dialog-button">Cancel</button>
        </div>
      </div>
    `;
    this.closeZones = this.dialogElement.querySelectorAll('.dialog-exit');
    this.confirmButton = this.dialogElement.querySelector('.dialog-confirm');
    this.cancelButton = this.dialogElement.querySelector('.dialog-cancel');
    document.body.appendChild(this.dialogElement);
  }

  _destroyDialog() {
    this.dialogElement.classList.remove('open');
    document.body.removeChild(this.dialogElement);
    delete this;
  }

  confirm() {
    return new Promise((resolve, reject) => {
      if (!this.dialogElement || !this.confirmButton || !this.cancelButton || !this.closeZones) {
        return reject('Error during dialog setup!');
      }
      this.closeZones.forEach(exit => {
        exit.addEventListener('click', event => {
          resolve(null);
          this._destroyDialog();
        });
      });
      this.confirmButton.addEventListener('click', event => {
        resolve(true);
        this._destroyDialog();
      });
      this.cancelButton.addEventListener('click', event => {
        resolve(false);
        this._destroyDialog();
      });
      this.dialogElement.classList.add('open');
    });
  }
}
